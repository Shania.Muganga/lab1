package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;


public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */

        new RockPaperScissors().run();

        
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        
        while(true) {
            System.out.println("Let's play round " + roundCounter);
            System.out.println("Your choice (Rock/Paper/Scissors)?");
            String playerMove = sc.nextLine();

            if(!playerMove.equals("rock") && !playerMove.equals("paper") && !playerMove.equals("scissors")){
                System.out.println("I do not understand cardboard. Could you try again?");
                }

        // Getting input from the computer
            else{
                int randomNumber = (int) (Math.random() *3);

                String computerMove = "";

                if (randomNumber == 0) {
                    computerMove = "rock";
                } else if (randomNumber == 1) {
                    computerMove = "paper";
                } else {
                    computerMove = "scissors";
                }

        // Print results
        if (playerMove.equals(computerMove)) {
            System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". It's a draw!");
        }
        else if((playerMove.equals("rock") && computerMove.equals("scissors")) 
        | (playerMove.equals("scissors") && computerMove.equals("paper")) 
        | (playerMove.equals("paper") && computerMove.equals("rock")) ){
            System.out.println("Human chose " + playerMove + ", computer chose "+ computerMove + ". Human wins!");
            humanScore += 1;
        }
        else{
            System.out.println("Human chose " + playerMove + ", computer chose "+ computerMove + ". Computer wins!");
            computerScore += 1;
        }

        System.out.println("Score: human " + playerMove + ", computer " + computerMove);
        roundCounter += 1;

        System.out.println("Do you wish to continue playing? (y/n)?");
        String svar = sc.nextLine();
        
        if(svar.equals("y")){
            continue;
            }
        else if(svar.equals("n")){
            System.out.println("Bye bye :) ");
            break;
            }
        }
}
    }

        

        



    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
